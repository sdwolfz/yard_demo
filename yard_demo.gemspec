# frozen_string_literal: true

require_relative 'lib/yard_demo/version'

Gem::Specification.new do |spec|
  spec.name = 'yard_demo'
  spec.version = YardDemo::VERSION
  spec.authors = ['Codruț Constantin Gușoi']
  spec.email = ['mail+git@codrut.pro']

  spec.summary = 'Yard gem demo usage'
  spec.description = 'How to use Yard to generate documentation in CI'
  spec.homepage = 'https://gitlab.com/sdwolfz/yard_demo'
  spec.license = 'BSD-3-Clause'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  spec.files = []
  spec.require_paths = ['lib']

  spec.add_development_dependency 'irb',              '~> 1.4'
  spec.add_development_dependency 'minitest',         '~> 5.16'
  spec.add_development_dependency 'pry-byebug',       '~> 3.9'
  spec.add_development_dependency 'rake',             '~> 13.0'
  spec.add_development_dependency 'rdoc',             '~> 6.4'
  spec.add_development_dependency 'rubocop',          '~> 1.32'
  spec.add_development_dependency 'rubocop-minitest', '~> 0.21'
  spec.add_development_dependency 'rubocop-rake',     '~> 0.6'
  spec.add_development_dependency 'simplecov',        '~> 0.21'
  spec.add_development_dependency 'yard',             '~> 0.9'
  spec.metadata['rubygems_mfa_required'] = 'true'
end
