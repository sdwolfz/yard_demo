# frozen_string_literal: true

require_relative 'yard_demo/example'
require_relative 'yard_demo/version'

# A demo of Yard!
module YardDemo
  # This is an error...
  class Error < StandardError; end
end
