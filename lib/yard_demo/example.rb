# frozen_string_literal: true

module YardDemo
  # Class definition example.
  #
  # @api public
  # @since 0.0.1
  #
  # Usage:
  #
  # ```ruby
  # YardDemo::Example.new('Example 1')
  # ```
  class Example
    # A name is given and taken
    attr_accessor :name

    # Constructors construct things
    #
    # @param name [String] the name of this example
    # @return [void]
    def initialize(name)
      @name = name
    end
  end
end
